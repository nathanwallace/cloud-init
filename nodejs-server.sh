# Update server to the latest package versions
yum -y update

# Download
yum -y install git

# install boto
yum -y install http://dl.fedoraproject.org/pub/epel/testing/6/i386/python-boto-2.5.1-1.el6.noarch.rpm

touch ~/cloud-init-complete

# download the secure cloud-init script and execute
#download-cloud-init.py https://cloud-init.78th.st/nodejs-server
#nodejs-server/cloud-init-secure.sh
