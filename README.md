78th.st cloud-init configuration for EC2

Server installation follows a basic 3 step process:
1. Download and run a short publically accessible cloud-init script from BitBucket.
2. Download private keys from Amazon S3, protected by IAM role access.
3. Download files and run a cloud-init-secure script from BitBucket.

This allows new instances to do a full automated install from version controlled scripts while maintaining data security.
